﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planoController : MonoBehaviour
{
    private float contador;
    //Put this outside the update function
    public float rotateFactor = 1f;

//Put this inside the update function

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //    transform.rotation = new Quaternion((transform.rotation.x+1),0, (transform.rotation.z+1), transform.rotation.w);
        
        float rotateX = Input.GetAxisRaw("Horizontal") * rotateFactor;
        float rotateY = Input.GetAxisRaw("Vertical") * rotateFactor;
        transform.eulerAngles += new Vector3(rotateX, 0, rotateY); //You may need to switch these around depending on your setup
    }

   /*
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Coleccionable"){
            contador+=1;
            Debug.Log(contador);
            Destroy(other);
        }

    }*/
}
