﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float speed;
    private Rigidbody _rb;
    private Vector3 movement;
    // Start is called before the first frame update
    void Start()
    {
        speed=100f;
        _rb=this.gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        movement=new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
        _rb.AddForce(movement*speed);
    }
}
